require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  def test_save_without_code
      country = Country.new(:name => 'India')
      assert_equal country.valid?, false
      assert_equal country.save, false
  end

  def test_save_without_name
      country = Country.new(:code => 'India')
      assert_equal country.valid?, false
      assert_equal country.save, false
  end

  def test_save_with_code_and_name
  	  country = Country.new(:code => 'id', :name => 'Indonesia' )
  	  assert_equal country.valid?, true
  	  assert_equal country.save, true
  end 	


  def test_save_with_unallowed_code
      country = Country.new(:code=>'jpn',:name=>'new_name')
      assert_equal country.valid?, false
      assert_equal country.save, false
  end

  def test_save_with_correct_code_and_name
    country = Country.new(:code=>'usa',:name=>'new_name')
    assert_equal country.valid?, true
    assert_equal country.save, true
  end

   def test_relation_between_country_and_user
    country = Country.create(:code=>'usa',:name=>'new_name')
    assert_not_nil country
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last', :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>country.id)
    user.encrypt_password
    user.save
    assert_not_nil country.users
    assert_equal country.users.empty?, false
    assert_equal country.users[0].class, User
  end

  
end
