require 'test_helper'

class ProductTest < ActiveSupport::TestCase

  def test_save_without_name
      product = Product.new(:price => 1000, :stock => '10', :description => 'blackberry')
      assert_equal product.valid?, false
      assert_equal product.save, false
  end

  def test_save_without_price
      product = Product.new(:name => 'blackberry', :stock => '10', :description => 'blackberry')
      assert_equal product.valid?, false
      assert_equal product.save, false
  end

  def test_save_without_stock
      product = Product.new(:price => 1000, :name => 'blackberry', :description => 'blackberry')
      assert_equal product.valid?, false
      assert_equal product.save, false
  end

  def test_save_without_description
      product = Product.new(:price => 1000, :stock => '10', :name => 'blackberry')
      assert_equal product.valid?, false
      assert_equal product.save, false
  end

  def test_save_with_all_field
      product = Product.new(:name => 'blackberry', :price => 1000, :stock => '10', :description => 'blackberry')
      assert_equal product.valid?, true
      assert_equal product.save, true
  end

  def test_save_with_price_not_nummeric
      product = Product.new(:price => 'fdsfdsf', :stock => '10', :name => 'blackberry', :description => 'fdfds')
      assert_equal product.valid?, false
      assert_equal product.save, false
  end

  def test_relation_between_category_and_product
    category = Category.create(:name=>'new_cat',:user_id=>'2')
    assert_not_nil category
    product = Product.create(:name => 'new_prod', :price => 20000,:stock=>32,:description=>"new_desc",:user_id=>"2")
    assert_not_nil product
    cat_prod = CategoriesProducts.create(:category_id=>category.id,:product_id=>product.id)
    assert_not_nil category.products
    assert_equal category.products.empty?, false
    assert_equal category.products[0].class, Product
  end

  def test_cek_stock
   	Product.create([
    	{:name => "new product 1", :price => 20000,:stock=>'2',:description=>"new_desc",:user_id=>"2"}, 
    	{:name => "new product 1", :price => 20000,:stock=>'1',:description=>"new_desc",:user_id=>"2"}
   	])
   	assert_not_nil Product.cek_stock(1)
  end

  def test_cek_price
   	Product.create([
    	{:name => "new product 1", :price => 50000,:stock=>'2',:description=>"new_desc",:user_id=>"2"}, 
    	{:name => "new product 1", :price => 20000,:stock=>'1',:description=>"new_desc",:user_id=>"2"}
   	])
   	assert_not_nil Product.cek_stock(30000)
  end


end
