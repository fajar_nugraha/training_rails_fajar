require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  def test_save_without_name
      category = Category.new(:user_id => '2')
      assert_equal category.valid?, false
      assert_equal category.save, false
  end



  def test_save_with_name_and_user_id
    category = Category.new(:name => 'new_name', :user_id=>'2')
    assert_equal category.valid?, true
    assert_equal category.save, true
  end
 
  def test_find_books_category
    Category.create([
      {:name=>'new_category books',:user_id=>1}, {:name=>'new_title book',:user_id=>2},
    ])
    assert_not_nil Category.books
    assert_equal Category.books[0].name, 'new_category books'
  end

  def test_find_shoes
      category = Category.create(:name=>'new_cat',:user_id=>'2')
    assert_not_nil category
    product = Product.create(:name => 'Adidas shoes', :price => "20500",:stock=>323,:description=>"new_desc",:user_id=>"2")
    assert_not_nil product
    cat_prod = CategoriesProducts.create(:category_id => category.id, :product_id => product.id)
    assert_not_nil category.creator_categories
    assert_equal category.creator_categories.empty?, false
    assert_equal category.creator_categories[0].class, Product
  end

  def test_relation_between_category_and_product
    category = Category.create(:name=>'new_cat',:user_id=>'2')
    assert_not_nil category
    product = Product.create(:name => 'new_prod', :price => "20000",:stock=>32,:description=>"new_desc",:user_id=>"2")
    assert_not_nil product
    cat_prod = CategoriesProducts.create(:category_id=>category.id,:product_id=>product.id)
    assert_not_nil category.products
    assert_equal category.products.empty?, false
    assert_equal category.products[0].class, Product
  end
  


end
