require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  
  	def test_save_without_title
	    article = Article.new(:descriptions => 'new description')
	    assert_equal article.valid?, false
	    assert_equal article.save, false
  	end

  	def test_save_without_description
	    article = Article.new(:title => 'new title')
	    assert_equal article.valid?, false
	    assert_equal article.save, false
  	end

   	def test_save_with_title_and_description
	    article = Article.new(:title => "Testing", :descriptions => "This is description")
	    assert_equal article.valid?, true
	    assert_equal article.save, true
   	end

    def test_find_rating
   		Article.create([
     		{:title => "article tes 1", :descriptions => "test scope descriptions", :rating => 1}, 
     		{:title => "article tes 2", :descriptions => "test scope descriptions", :rating => 2}
   		])
   		assert_not_nil Article.rating(1)
 	end

 	def test_relation_between_article_and_comment
	   article = Article.create(:title => "new_title_comment", :descriptions => "new descriptions comment")
	   assert_not_nil article
	   comment = Comment.create(:article_id => article.id, :content => "my comment")
	   assert_not_nil article.comments
	   assert_equal article.comments.empty?, false
	   assert_equal article.comments[0].class, Comment
 	end

   

end
