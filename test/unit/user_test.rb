require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def test_save_without_first_name
      user = User.new(:last_name => 'oenyil', :email => 'oentil@gmail.com' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_without_last_name
      user = User.new(:first_name => 'fajar', :email => 'oentil@gmail.com' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_without_email
      user = User.new(:last_name => 'oenyil', :first_name => 'fajar' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_without_username
      user = User.new(:last_name => 'oenyil', :email => 'oentil@gmail.com' , :first_name => 'fajar', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_without_password
      user = User.new(:last_name => 'oenyil', :email => 'oentil@gmail.com' , :username => 'fajaroenyil', 
      				:first_name => 'fajar', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_without_date_of_birth
      user = User.new(:last_name => 'oenyil', :email => 'oentil@gmail.com' , :username => 'fajaroenyil', 
      				:password => '123', :first_name => 'fajar' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_without_age
      user = User.new(:last_name => 'oenyil', :email => 'oentil@gmail.com' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => 'fajar' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_without_address
      user = User.new(:last_name => 'oenyil', :email => 'oentil@gmail.com' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:first_name => 'fajar', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_with_all_field
      user = User.new(:first_name => 'fajar', :last_name => 'oenyil', :email => 'oentil@gmail.com' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, true
      assert_equal user.save, true
  end

  def test_save_with_all_field_and_cek_length
      user = User.new(:first_name => 'ssssssssssssssssssssssfajar', :last_name => 'ssssssssssssssssssssssfajar', :email => 'ssssssssssssssssssssssfajar' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_save_with_all_field_and_format_not_character
      user = User.new(:first_name => '23122', :last_name => '432324', :email => 'oenyil@gmai.com' , :username => 'fajaroenyil', 
      				:password => '123', :date_of_birth => '12 Mei 2014' , :age => '12' , 
      				:address => 'bandung', :country_id => '3' , :is_admin => 1)
      assert_equal user.valid?, false
      assert_equal user.save, false
  end

  def test_relation_between_category_and_product
    category = Category.create(:name=>'new_cat',:user_id=>'2')
    assert_not_nil category
    product = Product.create(:name => 'new_prod', :price => "20000",:stock=>32,:description=>"new_desc",:user_id=>"2")
    assert_not_nil product
    cat_prod = CategoriesProducts.create(:category_id=>category.id,:product_id=>product.id)
    assert_not_nil category.products
    assert_equal category.products.empty?, false
    assert_equal category.products[0].class, Product
  end

  def test_relation_between_user_and_article
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last', :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>1)
    user.encrypt_password
    user.save
    article = Article.create(:user_id => user.id, :title=>'title',:descriptions=>'new descriptions')
    assert_not_nil article
    
    assert_not_nil user.articles
    assert_equal user.articles.empty?, false
    assert_equal user.articles[0].class, Article
  end

  def test_relation_between_user_and_comment
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last', :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>1)
    user.encrypt_password
    user.save
    comment = Comment.create(:user_id => user.id, :content=>'bagusss',:article_id=>1)
    assert_not_nil comment
    assert_not_nil user.comments
    assert_equal user.comments.empty?, false
    assert_equal user.comments[0].class, Comment
  end

  def test_address_country
   	country = Country.create(:code=>'usa',:name=>'new_name')
    assert_not_nil country
   	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last', :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>country.id)
    user.encrypt_password
    user.save
   	assert_not_nil user.show_address_country
  end




end
