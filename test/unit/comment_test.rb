require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  def test_save_without_content
      comment = Comment.new(:user_id => '2', :article_id => '2')
      assert_equal comment.valid?, false
      assert_equal comment.save, false
  end

  def test_save_with_all_field
    comment = Comment.new(:user_id => '2', :article_id => '2', :content => "bagussss")
    assert_equal comment.valid?, true
    assert_equal comment.save, true
  end

end
