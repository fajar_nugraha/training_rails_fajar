require 'test_helper'

class CategoriesControllerTest < ActionController::TestCase
  def setup
    	login_as('oenyil91@gmai.com')
	    @controller = CategoriesController.new
	    @request    = ActionController::TestRequest.new
	    @response   = ActionController::TestResponse.new
    end

    def test_index
    	login_as('oenyil91@gmai.com')
	    get :index
	    assert_response :redirect
	    assert_not_nil assigns(:category)
  	end
end
