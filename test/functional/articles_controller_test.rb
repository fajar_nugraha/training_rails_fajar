require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
    def setup
    	login_as('oenyil91@gmai.com')
	    @controller = ArticlesController.new
	    @request    = ActionController::TestRequest.new
	    @response   = ActionController::TestResponse.new
    end

    def test_index
    	login_as('oenyil91@gmai.com')
	    get :index
	    assert_response :redirect
	    assert_nil assigns(:articles)
  	end

  	def test_new
  		login_as('oenyil91@gmai.com')
	    get :new
	    assert_nil assigns(:article)
	    assert_response :redirect
  	end

  	def test_create
      	login_as('oenyil91@gmai.com')
          assert_difference('Article.count') do
          post :create, :article => {:title=>'new_title',:descriptions=>'new_desc',:rating=>'1',:user_id=>'2'}
          assert_equal assigns(:article).title, "new_title"
          assert_equal assigns(:article).valid?, true
    end
	    assert_response :redirect
		####assert_redirected_to article_path(assigns(:articles))
	    assert_equal flash[:notice], 'Article was successfully created.'
  	end


  	def test_show
	    login_as('oenyil91@gmai.com')
	    get :show, :id => Article.first.id
	    assert_not_nil assigns(:article)
	    assert_response :success
  	end

  	def test_show_with_undefined_id
  		login_as('oenyil91@gmai.com')
	    #get :show, :id => nil
	    assert_nil assigns(:article)
	    #assert_response :redirect
	    #assert_redirected_to articles_path
	    #    assert_equal flash[:notice], 'Cannot find the article'
  	end

  	def test_edit
  		login_as('oenyil91@gmai.com')
	    get :edit, :id => Article.first.id
	    assert_not_nil assigns(:article)
	    assert_response :success
  	end

end
