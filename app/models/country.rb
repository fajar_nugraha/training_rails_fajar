class Country < ActiveRecord::Base
  attr_accessible :code, :name, :user_id
  validates :code, :presence => { :message => "You must be filled 'code' "}
  validates :name, :presence => { :message => "You must be filled 'name' "}
  validate :valid_name
  has_many :users
  has_many :creator_countries,
  			:class_name => "User",
  			 	:foreign_key => "country_id",
  			 	:conditions => "country_id = 6 "

  def valid_name
  	self.errors[:code] << "Can't filled by text" unless (code == 'ID' || code == 'USA' || code == 'CHN' )
  end

end
