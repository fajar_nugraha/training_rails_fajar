class Article < ActiveRecord::Base
  attr_accessible :title, :descriptions, :rating, :user_id
  validates :title, :presence => { :message => "You must be filled 'title' "},
  					:uniqueness => true
  validates :descriptions, :presence => { :message => "You must be filled 'descriptions' "}
  belongs_to :user
  has_many :comments, :dependent => :destroy
  scope :rating, lambda {|time| where("rating < ?", time) }
  
  def self.show_one_hundred()
  	where("length(descriptions) > 100")
  end

end
