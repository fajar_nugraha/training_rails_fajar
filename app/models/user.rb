class User < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :email, :username, :password, :date_of_birth, :age, :address, :country_id, :password_confirmation, :is_admin
  attr_accessor :password
  before_save :encrypt_password

  validates :first_name, :presence => { :message => "You must be filled 'first name' "}, 
  						 :length => {:minimum => 0, :maximum => 25}, 
  						 :format => { :with => /[a-zA-Z\s]+$/ },
  						 :uniqueness => true
  validates :last_name, :presence => { :message => "You must be filled 'last name' "}, 
  						:length => {:minimum => 0, :maximum => 25}, 
  						:format => { :with => /[a-zA-Z\s]+$/ },
  						:uniqueness => true
  validates :email, :presence => { :message => "You must be filled 'email' "},
  					:uniqueness => true
  validates :username, :presence => { :message => "You must be filled 'username' "}
  validates :password, :presence => { :on => :create },
                        :confirmation => true
  validates :date_of_birth, :presence => { :message => "You must be filled 'date_of_birth' "}	
  validates :age, :presence => { :message => "You must be filled 'age' "}				
  validates :address, :presence => { :message => "You must be filled 'address' "}

  has_many :products
  has_many :articles
  belongs_to :country
  has_many :categories
  has_many :comments

  has_many :creator,
  			:class_name => "Article",
  			 	:foreign_key => "user_id",
  			 	:conditions => "title like '%my country%' "

  

  #default_scope where("country_id = 6")

  def show_address_country
    country = Country.find(self.country_id)
    "#{self.address} - #{country.name} "
  end

  def self.show_age
  	where("age > 18")
  end

  def encrypt_password
  if password.present?
          self.password_salt = BCrypt::Engine.generate_salt
          self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
     end
  end

  def self.authenticate(email, password)
    user = find_by_email(email)
      if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
        user
      else
        nil
      end
  end

end
