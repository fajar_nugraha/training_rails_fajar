class Category < ActiveRecord::Base
  attr_accessible :name, :user_id
  validates :name, :presence => { :message => "You must be filled 'name' "}
  belongs_to :user
  has_and_belongs_to_many :products
  has_many :categories_products
  has_and_belongs_to_many :creator_categories,
  			:class_name => "Product",
  			 	:foreign_key => "category_id",
  			 	:conditions => "name like '%shoes%' "
  #scope :cari, lambda {|time| where("name LIKE '%#{name}%' ", time) }
  scope :books, where("name like '%books%'")
end
