class Product < ActiveRecord::Base
  attr_accessible :name, :price, :stock, :description, :user_id
  validates :name, :presence => { :message => "You must be filled 'name' "}
  validates :price, :numericality => true, :presence => { :message => "You must be filled 'price' "}
  validates :stock, :presence => { :message => "You must be filled 'stock' "}
  validates :description, :presence => { :message => "You must be filled 'description' "}
  belongs_to :user
  has_and_belongs_to_many :categories
  #has_many :categories_products
  scope :cek_stock, lambda {|time| where("stock = ?", time) }
  scope :cek_price, lambda {|time| where("price < ?", time) }
  scope :search_name, lambda {|time| where( "name = ? ", time ) } 

  def calculate_age
    # => product = Product.find(id)
    coba = self.created_at
    coba_1 = coba.to_date
    date_now = Date.today.to_date
    year_now = date_now.year
    month_now = date_now.mon
    
    birthday_year = coba_1.year
    birthday_month = coba_1.mon
    lama_thn = (year_now - birthday_year) / 365

    thn = year_now - birthday_year
    mon = month_now - birthday_month
    
    puts "Lama bekerja : #{thn} Tahun, where is #{month_now} #{birthday_month} ?"

  end

end
