class Comment < ActiveRecord::Base
  attr_accessible :content, :user_id, :article_id
  validates :content, :presence => { :message => "You must be filled 'content' "}
  belongs_to :user
  belongs_to :article
end
