class CountriesController < ApplicationController
before_filter :require_login, :only => [:new, :create, :edit, :update, :delete, :index]
	def new
		@country = Country.new
	end


	def index
		# ini bisa
		@country = Country.all
		#ini juga bisa
		#@country = Country.where("code IN ('id', 'usa') ", 18).paginate(:page => params[:page], :per_page => 5, :order => 'id ASC')
	end


	def create
		@country = Country.new(params[:country])
      if @country.save
      	 	flash[:notice] = 'User was successfully created.'
            redirect_to :action => 'index'
      else
      		flash[:error] = 'User was failed to create.'
      		redirect_to :action => 'new'
      end
	end


	def edit
		@country = Country.find(params[:id])		
	end


	def update
		@country = Country.find(params[:id])
      if @country.update_attributes(params[:country])
      	 flash[:notice] = 'User was successfully updated.'
         redirect_to :action => 'index', :id => @country
      else
      	 flash[:error] = 'User was failed to updated.'
         render :action => 'edit'
      end		
	end


	def show
		@country = Country.find(params[:id])
	end


	def destroy
		@country = Country.find(params[:id])
 		@country.destroy
 		redirect_to :action => 'index'
	end

end
