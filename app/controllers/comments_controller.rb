class CommentsController < ApplicationController
before_filter :require_login, :only => [:index,:new, :create, :edit, :update, :destroy]
before_filter :find_comment, :only => [:edit, :update, :show, :destroy]

  def index
    @comments = Comment.find(:all)
  end

  def new
    @comment = Comment.new
  end

  def create
  	@comment = Comment.new(params[:comment])
  	respond_to do |format|
	   if @comment.save
	  	format.html { redirect_to(article_path(article), :notice => 'Comment was successfully created.') }
      format.js { @comments = Article.find(params[:comment][:article_id].to_i).comments }
	  end
	  end
    
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def update
    @comment = Comment.find(params[:id])
    if @comment.update_attributes(params[:comment])
      flash[:notice] = 'Comment was successfully updated.'
      redirect_to(@comment)
    else
      flash[:error] = 'Comment was failed to update.'
      render :action => "edit"
    end
  end

  def show
      @comment = Comment.find(params[:id])
  end

  def destroy
    @comment = Comment.find(params[:id])
    if @comment.user_id == session[:user_id]
      #flash[:notice] = "Comment is deleted."
      @comment.destroy ? (flash[:error] = "Comment successfully deleted") :
                    (flash[:notice] = "Comment failed to delete")
      redirect_to comments_url
    else
      @comment.destroy ? (flash[:error] = "Comment successfully deleted") :
                    (flash[:notice] = "Comment failed to delete")
      redirect_to comments_url
    end
  end

  private
    def find_comment
      @comment = Comment.find_by_id(params[:id])
      if @comment.nil?
        flash[:notice] = "Cannot find the comment"
        redirect_to comments_url
      end
    end

end
