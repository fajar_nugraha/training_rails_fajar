class UsersController < ApplicationController
before_filter :require_login, :only => [:index, :update, :edit, :destroy, :show]

   	def new
		@user = User.new
		@country = Country.all
	end


	def index
		@user = User.all
	end


	def create
		@user = User.new(params[:user])

      	if verify_recaptcha
	    	if @user.save
	      		UserMailer.registration_confirmation(@user).deliver
	      		#flash[:notice] = 'User was successfully created.'
	            redirect_to log_in_path, :notice => "Signed up!" 
	      	else
	      		flash[:error] = 'User was failed to create.'
	      		redirect_to :action => 'new'
	      	end
	    else
	    	flash[:error] = "There was an error with the recaptcha code below. Please re-enter the code and click submit."
			render "new"
		end
	end


	def edit
		@user = User.find(params[:id])		
	end


	def update
		@user = User.find(params[:id])
      if @user.update_attributes(params[:user])
      	 flash[:notice] = 'User was successfully updated.'
         redirect_to :action => 'index', :id => @user
      else
      	 flash[:error] = 'User was failed to updated.'
         render :action => 'edit'
      end		
	end


	def show
		@user = User.find(params[:id])
		@check_country = @user.show_address_country
	end


	def destroy
		@user = User.find(params[:id])
 		@user.destroy
 		redirect_to :action => 'index'
	end
end
