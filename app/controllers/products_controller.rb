class ProductsController < ApplicationController
before_filter :require_login, :only => [:new, :create, :edit, :update, :delete, :index]
	def new
		@product = Product.new
	end


	def index
		@product = Product.all
	end


	def create
		@product = Product.new(params[:product])
      if @product.save
      		flash[:notice] = 'User was successfully created.'
            redirect_to :action => 'index'
      else
      		flash[:error] = 'User was failed to create.'
      		redirect_to :action => 'new'
      end
	end


	def edit
		@product = Product.find(params[:id])		
	end


	def update
		@product = Product.find(params[:id])
      if @product.update_attributes(params[:product])
      	 flash[:notice] = 'User was successfully updated.'
         redirect_to :action => 'index', :id => @product
      else
      	 flash[:error] = 'User was failed to updated.'
         render :action => 'edit'
      end		
	end


	def show
		@product = Product.find(params[:id])
	end


	def destroy
		@product = Product.find(params[:id])
 		@product.destroy
 		redirect_to :action => 'index'
	end


end
