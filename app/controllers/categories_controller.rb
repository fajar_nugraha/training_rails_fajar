class CategoriesController < ApplicationController
before_filter :require_login, :only => [:new, :create, :edit, :update, :delete, :index]
	def new
		@category = Category.new
	end


	def index
		@category = Category.all
	end


	def create
		@category = Category.new(params[:category])
      if @category.save
      		flash[:notice] = 'User was successfully created.'
            redirect_to :action => 'index'
      else
      		flash[:error] = 'User was failed to create.'
      		redirect_to :action => 'new'
      end
	end


	def edit
		@category = Category.find(params[:id])		
	end


	def update
		@category = Category.find(params[:id])
      if @category.update_attributes(params[:category])
      	 flash[:notice] = 'User was successfully updated.'
         redirect_to :action => 'index', :id => @category
      else
      	 flash[:error] = 'User was failed to updated.'
         render :action => 'edit'
      end		
	end


	def show
		@category = Category.find(params[:id])
	end


	def destroy
		@category = Category.find(params[:id])
 		@category.destroy
 		redirect_to :action => 'index'
	end

end
