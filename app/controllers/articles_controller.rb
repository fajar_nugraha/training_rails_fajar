class ArticlesController < ApplicationController
before_filter :require_login, :only => [:new, :create, :edit, :update, :delete, :index]	
	def new
		@article = Article.new
	end


	def index
		@articles = Article.all
	end


	def create
		@article = Article.new(params[:article])
      if @article.save
      		flash[:notice] = 'User was successfully created.'
      	    redirect_to :action => 'index'
      else
      		flash[:error] = 'User was failed to create.'
      		redirect_to :action => 'new'
      end
	end


	def edit
		@article = Article.find(params[:id])
		if @article.user_id == session[:user_id]
			@article = Article.find(params[:id])	
		else
			flash[:notice] = "You are not permitted."
 			redirect_to articles_url
		end	
	end


	def update
		@article = Article.find(params[:id])
      if @article.update_attributes(params[:article])
      	 flash[:notice] = 'User was successfully updated.'
         redirect_to :action => 'index'
      else
      	 flash[:error] = 'User was failed to updated.'
         render :action => 'edit'
      end		
	end


	def show
		@comments = Comment.find_all_by_article_id(params[:id])
		@comment = Comment.new
		@article = Article.find(params[:id])
	end


	def destroy
		@article = Article.find(params[:id])
 		if @article.user_id == session[:user_id]
 			@article.destroy ? (flash[:error] = "Article successfully deleted") : (flash[:notice] = "Article failed to delete")
 			redirect_to articles_url
 		else
 			#redirect_to :action => 'index'
 			flash[:notice] = "Article failed to delete"
 			redirect_to articles_url
 		end
	end

end
