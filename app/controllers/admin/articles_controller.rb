class Admin::ArticlesController < ApplicationController
before_filter :require_admin_login, :only => [:new, :create, :edit, :update, :delete, :index]	

	def new
		#debugger
		@article = Article.new
	end


	def index
		@article = Article.all
		ap @article
	end


	def create
		@article = Article.new(params[:article])
      if @article.save
      		flash[:notice] = 'User was successfully created.'
      	    redirect_to :action => 'index'
      else
      		flash[:error] = 'User was failed to create.'
      		redirect_to :action => 'new'
      end
	end


	def edit
		@article = Article.find(params[:id])
	end


	def update
		@article = Article.find(params[:id])
      if @article.update_attributes(params[:article])
      	 flash[:notice] = 'User was successfully updated.'
         redirect_to :action => 'index', :controller => 'admin/articles'
      else
      	 flash[:error] = 'User was failed to updated.'
         render :action => 'edit'
      end		
	end


	def show
		@comments = Comment.find_all_by_article_id(params[:id])
		@comment = Comment.new
		@article = Article.find(params[:id])
	end


	def destroy
		@article = Article.find(params[:id])
 		
 			@article.destroy ? (flash[:error] = "Article successfully deleted") : (flash[:notice] = "Article failed to delete")
 			redirect_to admin_articles_url
 	end

end
