module ApplicationHelper
	def welcome_text
	  str = "" # if the user has logged in, show the welcome text.
	  if current_user
	    str = "Welcome, #{current_user.email} | "
	    str += link_to "Logout", log_out_path
	  else
	    str = "#{link_to "Login", log_in_path} | "
	    str += link_to "Signup", sign_up_path
	  end
	end 

	def menu
	  str = "" # if the user has logged in, show menu.
	  if current_user
	    str = "<p><h3> "
	    str += link_to "Article", {:controller => "articles", :action => "index"}
	    str += " | "
	    str += link_to "Product", {:controller => "products", :action => "index"}
	    str += " | "
	    str += link_to "Category", {:controller => "categories", :action => "index"}
	    str += " | "
	    str += link_to "Country", {:controller => "countries", :action => "index"}
	    str += " </h3></p> "

	  else
	    str = " "
	  end
	end 
end
