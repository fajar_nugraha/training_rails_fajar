# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = User.create([ {:first_name => "Fajar", :last_name => "Nugraha", :email => "fajar.nugraha@wgs.co.id"}, {:first_name => "Mega", :last_name => "Risyanti", :email => "mega.risyanti@wgs.co.id"}, {:first_name => "Bintang", :last_name => "Saputra", :email => "bintang.saputra@wgs.co.id"}, {:first_name => "Anggara", :last_name => "Siswanajaya", :email => "anggara@wgs.co.id"}, {:first_name => "Asrul", :last_name => "Syahrin", :email => "asrul.syahrin@wgs.co.id"} ])

articles = Article.create([ {:title => "Pengalaman Kerja 1", :body => "Lorem Ipsum Lorem Ipsum Lorem Ipsum"}, {:title => "Pengalaman Kerja 2", :body => "Lorem Ipsum Lorem Ipsum Lorem Ipsum"}, {:title => "Pengalaman Kerja 3", :body => "Lorem Ipsum Lorem Ipsum Lorem Ipsum"}, {:title => "Pengalaman Kerja 4", :body => "Lorem Ipsum Lorem Ipsum Lorem Ipsum"}, {:title => "Pengalaman Kerja 5", :body => "Lorem Ipsum Lorem Ipsum Lorem Ipsum"} ])

countries = Country.create([ {:code => "BDG", :name =>"Bandung"}, {:code => "JKT", :name =>"Jakarta"}, {:code => "SBY", :name =>"Surabaya"}, {:code => "PLM", :name =>"Palembang"}, {:code => "BGR", :name =>"Bogor"} ]) 

comments = Comment.create([ {:content =>"Bagus sekali artikel 1"}, {:content =>"Bagus sekali artikel 2"}, {:content =>"Bagus sekali artikel 3"}, {:content =>"Bagus sekali artikel 4"}, {:content =>"Bagus sekali artikel 5"} ]) 

