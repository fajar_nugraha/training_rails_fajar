class UpdateColumnCatPro < ActiveRecord::Migration
  def up
	add_column :category_products, :category_id, :integer
	add_column :category_products, :product_id, :integer
	remove_column :category_products, :id
  end

  def down
  end
end
