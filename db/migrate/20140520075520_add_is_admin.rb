class AddIsAdmin < ActiveRecord::Migration
  def up
	add_column :users, :is_admin, :boolean, default: false
  end

  def down
  end
end
