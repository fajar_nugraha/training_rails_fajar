class AddUserId < ActiveRecord::Migration
  def up
    add_column :products, :user_id, :integer
    add_column :articles, :user_id, :integer
    
  end

  def down
  end
end
