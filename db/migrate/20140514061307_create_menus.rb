class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
		t.string :menu_name
		t.string :controller
		t.string :url
		t.timestamps
    end
  end
end
