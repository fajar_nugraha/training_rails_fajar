class AddUserIdField < ActiveRecord::Migration
  def up
  	add_column :categories, :user_id, :integer
  	add_column :comments, :user_id, :integer
  	add_column :countries, :user_id, :integer
  end

  def down
  end
end
