#Create
2.0.0-p451 :001 > users = Users.create ({ :first_name => "Tes", :last_name => "Console", :email => "tes.console@gmail.com" })
   (0.2ms)  BEGIN
  SQL (0.4ms)  INSERT INTO `users` (`address`, `age`, `created_at`, `date_or_birth`, `email`, `first_name`, `last_name`, `password`, `updated_at`, `username`) VALUES (NULL, NULL, '2014-05-08 03:02:37', NULL, 'tes.console@gmail.com', 'Tes', 'Console', NULL, '2014-05-08 03:02:37', NULL)
   (44.8ms)  COMMIT
 => #<Users id: 11, first_name: "Tes", last_name: "Console", email: "tes.console@gmail.com", username: nil, password: nil, created_at: "2014-05-08 03:02:37", updated_at: "2014-05-08 03:02:37", date_or_birth: nil, age: nil, address: nil> 

#Select where id
2.0.0-p451 :002 > @user = Users.find(1)
  Users Load (0.5ms)  SELECT `users`.* FROM `users` WHERE `users`.`id` = 6 LIMIT 1
 => #<Users id: 6, first_name: "Fajar", last_name: "Nugraha", email: "fajar.nugraha@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil> 

#Select where kriteria
2.0.0-p451 :004 > @user = Users.where("last_name = 'saputra'")
  Users Load (0.6ms)  SELECT `users`.* FROM `users` WHERE (last_name = 'saputra')
 => [#<Users id: 8, first_name: "Bintang", last_name: "Saputra", email: "bintang.saputra@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil>] 

#Select find_by
2.0.0-p451 :008 > @users = Users.find_by_first_name("Mega")
  Users Load (0.4ms)  SELECT `users`.* FROM `users` WHERE `users`.`first_name` = 'Mega' LIMIT 1
 => #<Users id: 7, first_name: "Mega", last_name: "Risyanti", email: "mega.risyanti@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil> 

2.0.0-p451 :010 > @users = Users.find_all_by_first_name_and_email("Fajar", "fajar.nugraha@wgs.co.id")
  Users Load (0.6ms)  SELECT `users`.* FROM `users` WHERE `users`.`first_name` = 'Fajar' AND `users`.`email` = 'fajar.nugraha@wgs.co.id'
 => [#<Users id: 6, first_name: "Fajar", last_name: "Nugraha", email: "fajar.nugraha@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil>] 

#Select Asc Desc
2.0.0-p451 :005 > Users.first!
  Users Load (0.5ms)  SELECT `users`.* FROM `users` LIMIT 1
 => #<Users id: 6, first_name: "Fajar", last_name: "Nugraha", email: "fajar.nugraha@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil> 
2.0.0-p451 :006 > Users.last!
  Users Load (0.5ms)  SELECT `users`.* FROM `users` ORDER BY `users`.`id` DESC LIMIT 1
 => #<Users id: 11, first_name: "Tes", last_name: "Console", email: "tes.console@gmail.com", username: nil, password: nil, created_at: "2014-05-08 03:02:37", updated_at: "2014-05-08 03:02:37", date_or_birth: nil, age: nil, address: nil> 

#Select column static
2.0.0-p451 :011 > average = Users.average(:id)
   (32.1ms)  SELECT AVG(`users`.`id`) AS avg_id FROM `users` 
 => #<BigDecimal:affc4b8,'0.85E1',18(18)> 

#Select by sql query
2.0.0-p451 :012 > users = Users.find_by_sql("select * from users where first_name in ('Fajar', 'Mega') ")
  Users Load (19.1ms)  select * from users where first_name in ('Fajar', 'Mega') 
 => [#<Users id: 6, first_name: "Fajar", last_name: "Nugraha", email: "fajar.nugraha@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil>, #<Users id: 7, first_name: "Mega", last_name: "Risyanti", email: "mega.risyanti@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil>] 

#Update
2.0.0-p451 :013 > users = Users.find_by_id(6)
  Users Load (0.5ms)  SELECT `users`.* FROM `users` WHERE `users`.`id` = 6 LIMIT 1
 => #<Users id: 6, first_name: "Fajar", last_name: "Nugraha", email: "fajar.nugraha@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 02:17:29", date_or_birth: nil, age: nil, address: nil> 
2.0.0-p451 :014 > users.update_attribute(:last_name, "Oenyil")
   (0.4ms)  BEGIN
   (102.6ms)  UPDATE `users` SET `last_name` = 'Oenyil', `updated_at` = '2014-05-08 03:54:14' WHERE `users`.`id` = 6
   (44.3ms)  COMMIT
 => true 
2.0.0-p451 :015 > users = Users.update(7, :last_name => "Jigong")
  Users Load (1.3ms)  SELECT `users`.* FROM `users` WHERE `users`.`id` = 7 LIMIT 1
   (0.3ms)  BEGIN
   (1.4ms)  UPDATE `users` SET `last_name` = 'Jigong', `updated_at` = '2014-05-08 04:03:51' WHERE `users`.`id` = 7
   (44.1ms)  COMMIT
 => #<Users id: 7, first_name: "Mega", last_name: "Jigong", email: "mega.risyanti@wgs.co.id", username: nil, password: nil, created_at: "2014-05-08 02:17:29", updated_at: "2014-05-08 04:03:51", date_or_birth: nil, age: nil, address: nil> 

#Delete by id
2.0.0-p451 :016 > @users = Users.find_by_id(11)
  Users Load (1.2ms)  SELECT `users`.* FROM `users` WHERE `users`.`id` = 11 LIMIT 1
 => #<Users id: 11, first_name: "Tes", last_name: "Console", email: "tes.console@gmail.com", username: nil, password: nil, created_at: "2014-05-08 03:02:37", updated_at: "2014-05-08 03:02:37", date_or_birth: nil, age: nil, address: nil> 
2.0.0-p451 :017 > @users.destroy
   (0.4ms)  BEGIN
  SQL (1.1ms)  DELETE FROM `users` WHERE `users`.`id` = 11
   (59.1ms)  COMMIT
 => #<Users id: 11, first_name: "Tes", last_name: "Console", email: "tes.console@gmail.com", username: nil, password: nil, created_at: "2014-05-08 03:02:37", updated_at: "2014-05-08 03:02:37", date_or_birth: nil, age: nil, address: nil> 

#Delete where kriteria IN
2.0.0-p451 :018 > Users.delete([9,10])
  SQL (38.4ms)  DELETE FROM `users` WHERE `users`.`id` IN (9, 10)
 => 2 

#Delete with kriteria more than or less
2.0.0-p451 :019 > Comments.delete_all(["id > ?", 4])
  SQL (40.4ms)  DELETE FROM `comments` WHERE (id > 4)
 => 1 






